package kamil.HomeWork4.main;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        makeWrap();
    }

    public static void makeWrap() {
        Scanner scanner = new Scanner(System.in);
        boolean findTheNumber = false;
        System.out.println("Введите размер массива");
        int sizeArray = scanner.nextInt();
        int[] array = new int[sizeArray];
        int divisionCounter = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        Arrays.sort(array);

        System.out.println("Исходный массив " + Arrays.toString(array) + "\nВведите искомое число");

        int middle = 0;
        while (findTheNumber != true){


            int numberForSearch = scanner.nextInt();
            if (numberForSearch > array[array.length - 1] || numberForSearch < array[0]) {
                System.out.println("Введенное число отсутствует в массиве");
            } else {
                int result = binSearch(array, numberForSearch, array.length, array.length - 1, middle, divisionCounter);
                if (result == -1) {
                    System.out.println("Введенное число отсутствует в массиве");
                } else {
                    System.out.println("Индекс искомого числа равен: " + result);
                    findTheNumber = true;

                }
            }
        }
    }


    public static int binSearch(int[] array, int number, int from, int to, int middle, int divisionCounter) {

        middle = (from + to) / 2;
        if (divisionCounter > array.length) {
            return -1;
        }
        if (array[middle] > number) {
            divisionCounter++;
            return binSearch(array, number, from - 1, to - 1, middle - 1, divisionCounter);
        } else if (array[middle] < number) {
            divisionCounter++;
            return binSearch(array, number, from + 1, to + 1, middle + 1, divisionCounter);
        }
        return middle;
    }
}
