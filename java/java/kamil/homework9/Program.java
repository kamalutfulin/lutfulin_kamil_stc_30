package kamil.homework9;

import java.util.ArrayList;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        NumbersProcess n1 = new NumbersAndStringProcessor();
        StringsProcess n2 = new NumbersAndStringProcessor();
        NumbersAndStringProcessor n3 = (NumbersAndStringProcessor) n1;
        Object object = n3;
        Scanner scanner = new Scanner(System.in);

        NumbersProcess numbersProcess = number -> {
            int result = 0;
            while(number > 0) {
                result = result * 10 + number % 10;
                number /= 10;
            }
            return result;
        };
        System.out.println("Введите число");
        int a = numbersProcess.process(scanner.nextInt());
        System.out.println( "развернуть исходное число: " + a);
        NumberJoiner numberJoiner = new NumberJoiner();
        numberJoiner.process(6);
        NumberInverter numberInverter = new NumberInverter();
        numberInverter.process(10);
        ArrayList<NumbersProcess> numberProcesses = new ArrayList<>();
        numberProcesses.add(numberJoiner);
        numberProcesses.add(numberInverter);
        numberProcesses.add(number -> 0);
    }
    public static int process(ArrayList<NumbersProcess> numberProcesses, int number ){
        for (NumbersProcess numberProcess : numberProcesses) {
           number = numberProcess.process(number) ;
        }
        return number;
    }

}

