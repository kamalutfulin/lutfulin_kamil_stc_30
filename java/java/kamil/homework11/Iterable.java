package kamil.homework11;

public interface Iterable<C>{
    // FactoryMethod()
    Iterator<C> iterator();
}
