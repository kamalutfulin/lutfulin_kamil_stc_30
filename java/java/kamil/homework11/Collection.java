package kamil.homework11;

public interface Collection<B> extends Iterable<B> {
    void add(B element);
    boolean contains(B element);
    int size();
    void removeFirst(B element);
}

