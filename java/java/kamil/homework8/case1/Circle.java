package kamil.homework8.case1;

public class Circle extends GeometricFigure {
private int radius;

    public int getRadius() {
        return radius;
    }

    public Circle(String name, int radius, int x, int y) {
        super(name, x, y);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        double area = (Math.PI * Math.pow(getRadius(),2));
        System.out.println("Объект " + getName() + " имеет площадь: " + area);
        return area;
    }
    public void perimeter(){
        System.out.println("Объект "+getName()+ " имеет периметр " + 2*Math.PI*getRadius());
    }

    @Override
    public void changeSize(int a) {
radius = a;
        System.out.println("Объект " + getName() + " теперь имеет сторону размером : " + radius);
        getArea();
    }
}
