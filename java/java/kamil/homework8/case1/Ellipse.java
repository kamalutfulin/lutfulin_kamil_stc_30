package kamil.homework8.case1;

public class Ellipse  extends  GeometricFigure{
    private int semiMajorAxis;
    private int semiMinorAxis;

    public int getSemiMajorAxis() {
        return semiMajorAxis;
    }

    public void setSemiMajorAxis(int semiMajorAxis) {
        this.semiMajorAxis = semiMajorAxis;
    }
    public int getSemiMinorAxis() {
        return semiMinorAxis;
    }

    public void setSemiMinorAxis(int semiMinorAxis) {
        this.semiMinorAxis = semiMinorAxis;
    }


    public Ellipse(String name, int semiMajorAxis, int semiMinorAxis , int x, int y) {
        super(name, x, y);
        this.semiMajorAxis = semiMajorAxis;
        this.semiMinorAxis = semiMinorAxis;
    }


    @Override
    public double getArea() {
         double area = (Math.PI * getSemiMinorAxis()* getSemiMajorAxis());
        System.out.println("Объект " +  getName() + " имеет площадь: " + area);
        return area;
    }

    public void perimeter(){
        double result = 4 * ((Math.PI* getSemiMajorAxis()*semiMinorAxis+(getSemiMajorAxis()-semiMinorAxis))/(getSemiMajorAxis()+semiMinorAxis));
        System.out.println("Объект "+getName()+ " имеет периметр " +   result ) ;
    }
@Override
    public void changeSize(int a,int b) {
semiMinorAxis = a;
semiMajorAxis = b;
    System.out.println("Объект " + getName() + " теперь имеет малую ось : " + semiMinorAxis + " и большую ось : " + semiMajorAxis);
getArea();
    }
}
