package kamil.homework8.case1;

import java.awt.geom.Area;

public class Square extends GeometricFigure {
    private double side;


    public double getSide() {
        return side;
    }

    public Square(String name, int side, int x, int y) {
        super(name, x, y);
        this.side = side;
    }


    @Override
    public double getArea() {
        double area = side * side;
        System.out.println("Объект " + getName() + " имеет площадь: " + area);
        return area;
    }


    public void perimeter() {
        System.out.println("Объект " + getName() + " имеет периметр " + 4 * getSide());
    }

    @Override
    public void changeSize(int a) {
        this.side = a;
        System.out.println("Объект " + getName() + " теперь имеет сторону размером : " + side);
        getArea();
    }
}
