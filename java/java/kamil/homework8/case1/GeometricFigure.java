package kamil.homework8.case1;

public abstract class GeometricFigure implements Movable,Size{
    private String name;

    private int x = 0;
    private int y = 0;

   public  abstract double getArea() ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public GeometricFigure(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void area() { // TODO отвыкай писать методы которые ничего не возвращают: а то что от них джут печатают в консоль, это беспрлезная трата времени, и это нельзя использовать
        System.out.println("Запустится, если у фигуры нет своего метода");
    }

    public void perimeter() {
        System.out.println("Запустится, если у фигуры нет своего метода");
    }



    public void MovingFigure(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("Текущие координаты объекта " + getName() + " x:" + x + " y:" + y);
    }

    @Override
    public void MovingFigure() {
        System.out.println("Текущие координаты объекта " + getName() + " x:" + x + " y:" + y);
    }


    // TODO многовато методов, переопределение так не работает
    public void changeSize() {
        System.out.println("Запустится, если у фигуры нет своего метода");
    }
    public void changeSize(int a) {
        System.out.println("Запустится, если у фигуры нет своего метода");
    }
    public void changeSize(int a, int b) {
        System.out.println("Запустится, если у фигуры нет своего метода");
    }

}


