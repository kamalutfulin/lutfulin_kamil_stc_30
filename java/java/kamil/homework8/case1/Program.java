package kamil.homework8.case1;

import java.util.Scanner;

public class Program  {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        GeometricFigure circle = new Circle("Круг",10,0,0);
        GeometricFigure ellipse = new Ellipse("Эллипс", 4,3,0,0);
        GeometricFigure square = new Square("Квадрат",10,0,0);
        GeometricFigure rectangle = new Rectangle("Прямоугольник",10,5,0,0);
        square.getArea();
        square.changeSize(2);
        ellipse.getArea();
        ellipse.changeSize(5,6);
        circle.getArea();
        circle.changeSize(5);
        rectangle.getArea();
        rectangle.changeSize(5,4);
        }


    }