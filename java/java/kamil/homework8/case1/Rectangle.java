package kamil.homework8.case1;

public class Rectangle extends GeometricFigure {

    private int sideA;
    private int sideB;

    public int getSideA() {
        return sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public Rectangle(String name, int sideA, int sideB, int x, int y) {
        super(name, x, y);
        this.sideA = sideA;
        this.sideB = sideB;

    }


    @Override
    public double getArea() {
        double area = getSideA() * getSideB();
        System.out.println("Объект " + getName() + " имеет площадь: " + area);
        return area;
    }

    public void perimeter() {
        System.out.println("Объект " + getName() + " имеет периметр " + 2 * (getSideA() * getSideB()));
    }

    @Override
    public void changeSize(int a, int b) {
        sideA = a;
        sideB = b;
        System.out.println("Объект " + getName() + " теперь имеет стороны - A " + sideA + " и B " + sideB);
        getArea();
    }

}
