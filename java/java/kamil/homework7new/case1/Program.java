package kamil.homework7new.case1;

public class Program {

    public static void main(String[] args) {
        User user = new User.Builder()
                .firstName("Камиль")
                .lastName("Лутфулин")
                .age(26)
                .isWorker(true)
                .build();

    }
}

