package kamil.homework7new.case1;
public class User {

    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;
    private String build = "создан";

    public static class Builder {
        private User newUser;

        public Builder() {
            newUser = new User();
        }

        public Builder firstName(String name){
            newUser.firstName = name;
            return this;
        }

        public Builder lastName(String surname){
            newUser.lastName = surname;
            return this;
        }

        public Builder age(int age){
            newUser.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker){
            newUser.isWorker = isWorker;
            return this;
        }

        public Builder build(String build){
            newUser.build = build;
            return this;
        }

        public User build(){
            System.out.println(newUser.firstName+ " " + newUser.lastName + " " + " "+ newUser.age + " " + newUser.isWorker + " " + " " + newUser.build);
            return newUser;
        }


    }
}