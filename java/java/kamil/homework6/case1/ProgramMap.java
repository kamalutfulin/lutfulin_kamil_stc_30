package kamil.homework6.case1;

public class ProgramMap {


    public void selectAProgramm(String channel) {
        double a = Math.random() * 5;
        int randomNumber = (int) a;
        if (channel.equals("Первый")) {
            String[] programmsforchannel = {"Вечерний Ургант", "Смак", "Время", "Большие Гонки", "Путевые заметки"};
            String currentProgram = programmsforchannel[randomNumber];
            TV.showTvProgram(currentProgram,channel);
        }
        else if (channel.equals("MTV")) {
            String[] programmsForChannel = {"Бивис и Батхед", "Тачка на прокачку", "12 злобных зрителей", "Давай на спор", "Тихий час"};
            String currentProgram = programmsForChannel[randomNumber];
            TV.showTvProgram(currentProgram,channel);
        }
        else if (channel.equals("ТНТ")) {
            String[] programmsForChannel = {"Счастливы вместе", "Comedy Club", "Универ", "Эй, Арнольд", "Ох уж эти Детки!"};
            String currentProgram = programmsForChannel[randomNumber];
            TV.showTvProgram(currentProgram,channel);
        }
        else if (channel.equals("2x2")) {
            String[] programmsForChannel = {"Симпсоны", "Футурама", "Американский Папаша", "Рик и Морти", "Мистер Пиклз"};
            String currentProgram = programmsForChannel[randomNumber];
            TV.showTvProgram(currentProgram,channel);
        }


    }
}
