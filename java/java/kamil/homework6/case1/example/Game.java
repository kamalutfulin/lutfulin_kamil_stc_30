package kamil.homework6.case1.example;

public class Game {

    private Enemy[] allEnemies;
    private String name = "Shooter";

    public Game(int difficult) {
        allEnemies = new Enemy[difficult];
        for (int i = 0; i < allEnemies.length; i++) {
            allEnemies[i] = new Enemy("enemy" + i);
        }
    }

    public int getCountOfAliveEnemies(){
        int count = 0;
        for (Enemy enemy : allEnemies) {
            if (enemy.isAlive()){
                count++;
            }
        }
        return count;
    }

    public String getName() {
        return name;
    }

    public Enemy[] getAllEnemies() {
        return allEnemies;
    }
}
