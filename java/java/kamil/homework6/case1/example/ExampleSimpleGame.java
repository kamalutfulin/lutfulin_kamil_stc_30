package kamil.homework6.case1.example;

public class ExampleSimpleGame {
    public static void main(String[] args) {
        initContext();
    }

    public static void initContext(){
        ProGamer proGamer = new ProGamer();
        Game game = new Game(10);
        proGamer.startPlayGame(game);
    }
}
