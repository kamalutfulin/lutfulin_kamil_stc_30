package kamil.homework6.case1.example;

public class Enemy {

    private String name;
    private int x;
    private int y;
    private int hp;

    public Enemy(String name) {
        this.name = name;
        this.hp = (int) (Math.random() * 5);
    }

    public boolean die() {
        hp--;
        if (hp <= 0) {
            System.out.println("I am die");
            return true;
        } else {
            System.out.println("I am alive");
            return false;
        }
    }

    public boolean isAlive() {
        return hp > 0;
    }

    public String getName() {
        return name;
    }
}
