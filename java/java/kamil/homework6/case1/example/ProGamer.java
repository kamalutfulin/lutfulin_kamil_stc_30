package kamil.homework6.case1.example;

public class ProGamer {

    private Game currentGame;

    public void startPlayGame(Game game) {
        currentGame = game;
        sayPhrase("I am started play to game " + game.getName());
        win(currentGame);
    }

    private void win(Game game) {
        killAllEnemies();
        sayPhrase("I am win");
    }

    private void killAllEnemies() {
        while (currentGame.getCountOfAliveEnemies() > 0) {
            killEnemy(searchNextAliveEnemy());
        }
        sayPhrase("I am killed all enemies");
    }

    private void killEnemy(String enemyName) {
        Enemy enemy = searchEnemy(enemyName);
        if (enemy != null) {
            killEnemy(enemy);
            sayPhrase("I am kill " + enemy.getName());
        }
    }

    private void killEnemy(Enemy enemy) {
        sayPhrase("I am trying kill " + enemy.getName());
        enemy.die();
    }

    private Enemy searchNextAliveEnemy() {
        for (Enemy enemy : currentGame.getAllEnemies()) {
            if (enemy.isAlive()) {
                sayPhrase("I am found enemy with name" + enemy.getName());
                return enemy;
            }
        }
        sayPhrase("I am not found enemy alive enemies");
        return null;
    }

    private Enemy searchEnemy(String enemyName) {
        for (Enemy enemy : currentGame.getAllEnemies()) {
            if (enemy.getName() == enemyName) {
                sayPhrase("I am found enemy with name" + enemy.getName());
                return enemy;
            }
        }
        sayPhrase("I am not found enemy with name" + enemyName);
        return null;
    }

    private void sayPhrase(String phrase) {
        System.out.println(phrase);
    }
}
