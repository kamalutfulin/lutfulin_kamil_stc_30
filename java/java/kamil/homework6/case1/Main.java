package kamil.homework6.case1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        RemoteController remote1 = new RemoteController();
        TV tv1 = new TV();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите канал для просмотра! (Пока доступны с 1 - 4)");
        int inputButton = scanner.nextInt();
        remote1.sendSignalToTv(inputButton,tv1);

    }
}