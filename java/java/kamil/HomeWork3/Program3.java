package kamil.HomeWork3;

import java.util.Arrays;
import java.util.Scanner;

class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("������� ������ �������");
        int amountOfNumbers = scanner.nextInt();
        int array[] = new int[amountOfNumbers];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int cloneArray1[] = array.clone();
        int cloneArray2[] = array.clone();
        int cloneArray3[] = array.clone();


        System.out.println("����� ��������� �������: " + sum(array));
        System.out.println("�������� �������: " + back(cloneArray1, amountOfNumbers));
        System.out.println("������-��������������: " + average(array));
        System.out.println("������������ � ����������� ������� ���������� �������: " + maxAndMin(cloneArray2));
        System.out.println("���������� ������� ��������: " + bubble(cloneArray3));
        System.out.println("�������������� ������� � �����: " + arrayInNumber(array));
    }

    public static int sum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    public static String back(int[] array, int amountOfNumbers) {
        int sum = 0;
        int[] arrayBack = new int[amountOfNumbers];
        for (int i = 0; i < arrayBack.length; i++) {
            arrayBack[i] = array[array.length - 1 - i];
        }
        return Arrays.toString(arrayBack);
    }

    public static double average(int[] array) {
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        sum /= array.length;
        return sum;
    }

    public static String maxAndMin(int[] array) {
        int variable = 0;
        int indexOfMax = 0;
        int indexOfMin = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= array[indexOfMax]) {
                indexOfMax = i;
            } else if (array[i] < array[indexOfMax]) {
                if (array[i] < array[indexOfMin]) {
                    indexOfMin = i;
                }
            }
        }
        System.out.println("������������ ����� : " + array[indexOfMax] + " ; " + "����������� ����� : " + array[indexOfMin]);
        variable = array[indexOfMax];
        array[indexOfMax] = array[indexOfMin];
        array[indexOfMin] = variable;
        return Arrays.toString(array);
    }

    public static long arrayInNumber(int[] array) {
        long number = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < 10 && array[i] >= 0) {
                number = number * 10 + array[i];
            }
            if (array[i] >= 10 && array[i] < 100) {
                number = number * 100 + array[i];
            }
            if (array[i] >= 100 && array[i] < 1000) {
                number = number * 1000 + array[i];
            }
        }
        return number;
    }

    public static String bubble(int[] array) {
        int variable = 0;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1; j++) {

                if (array[j] > array[j + 1]) {
                    variable = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = variable;
                }
            }
        }
        return Arrays.toString(array);
    }
}