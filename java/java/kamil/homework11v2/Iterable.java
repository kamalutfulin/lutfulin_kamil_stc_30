package kamil.homework11v2;

public interface Iterable<C> {
    Iterator<C> iterator();
}
