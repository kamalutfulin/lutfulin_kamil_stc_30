package kamil.homework11v2;

public class MainLinkedList {

    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();

        list.add(14);
        list.add(88);
        list.add(19);
        list.add(95);
        list.add(58);
        list.add(2);
        list.add(1);

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        list.reverse();
        iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }
}

