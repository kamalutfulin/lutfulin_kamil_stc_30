package kamil.homework11v2;

public interface Iterator<A> {
    A next();

    boolean hasNext();
}
