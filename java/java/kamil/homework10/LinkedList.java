package kamil.homework10;

import com.sun.nio.sctp.IllegalUnbindException;

public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {

        @Override
        public int next() {
            return 0;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        throw new IndexOutOfBoundsException();


    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            System.err.println("Элемент не найден");
            throw new IllegalUnbindException();
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        int i = 0;
        Node current = this.first;
        while (current != null) {
            if ((i + 1) == index) {
                current.setNext(current.next.next);
                count--;
                return;
            } else {
                current = current.next;
                i++;
            }
        }
    }

    @Override
    public void insert(int element, int index) {
        Node current = this.first;
        Node current1 = current.next;
        Node newNode = new Node(element);
        if (index <= count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = newNode;
            newNode.next = current1;
            count++;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            count++;
        } else {
            System.out.println("Данного индекса нет!");
        }
    }


    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(int element) {
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
//        int i = 0;
//        Node current = this.first;
//        while (current != null) {
//            if ((i + 1) == index) {
//                current.setNext(current.next.next);
//                count--;
//                return;
//            } else {
//                current = current.next;
//                i++;
//            }
    }
    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
