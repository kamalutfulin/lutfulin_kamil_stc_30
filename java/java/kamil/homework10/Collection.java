package kamil.homework10;

public interface Collection extends Iterable {
    Object sort =true ;

    void add(int element);
    boolean contains(int element);
    int size();
    void removeFirst(int element);
}
