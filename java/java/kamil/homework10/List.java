package kamil.homework10;

import java.util.ListIterator;
import java.util.function.UnaryOperator;

public interface List extends Collection{
    int get(int index);
    int indexOf(int element);
    void removeByIndex(int index);
    void insert(int element, int index);
}
