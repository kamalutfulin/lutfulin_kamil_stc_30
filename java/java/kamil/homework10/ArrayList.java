package kamil.homework10;

public class ArrayList implements List {
    // контстанта для начального размера массива
    private static final int DEFAULT_SIZE = 10;
    // поле, представляющее собой массив, в котором мы храним элементы
    private int data[];
    // кол-во элементов в массиве (count != length)
    private int count;

    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public int get(int index) {
        // TODO нужно добавить проверку валидности индекса не только здесь (причем она "слабовата")
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");// TODO throw new IllegalArgumentException();
        return -1;
    }

    // 4, 5, 7, 8
    // indexOf(7) -> 2
    // indexOf(6) -> -1
    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        for (int i = index; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
    }

    @Override
    public void insert(int element, int index) {
        count++;
        if (count == data.length - 1) {
            resize();
        }
        for (int i = this.count; i > index; i--) {
            this.data[i] = this.data[i-1];
        }
        this.data[index] = element;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    /**
     * 10 -> 100
     * 15
     * <p>
     * 100 -> 110
     * 150
     * <p>
     * n -> n * 1.5
     */
    private void resize() {
        int oldLength = this.data.length;
        //  127(10) -> 1111111(2)
        //  1111111 >> 1 -> 0111111 -> 63
        // N >> K -> N / 2^K
        int newLength = oldLength + (oldLength >> 1); // oldLength + oldLength / 2;
        int newData[] = new int[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }


    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(int element) {
        int indexOfRemovingElement = indexOf(element);
        removeByIndex(indexOfRemovingElement);
//        for (int i = indexOfRemovingElement; i < count - 1; i++) {
//            this.data[i] = this.data[i + 1];
//        }
//
//        this.count--;
    }

    // реализация метода получения объекта
    @Override
    public Iterator iterator() {
        // CONCRETE PRODUCT
        return new ArrayListIterator();
    }
}

