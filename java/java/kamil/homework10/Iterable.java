package kamil.homework10;

public interface Iterable {
    // FactoryMethod()
    Iterator iterator();
}
