package kamil.homework12;

import java.util.HashMap;
import java.util.HashSet;

public class Program {
    public static void main(String[] args) {
        Map<String,Integer>map = new HashMapImpl<>();
        map.put("Аркадий",15);
        map.put("Иннокентий",15);
        map.put("Иннокентий",22);
        map.put("Валера",66);
        System.out.println(map.get("Аркадий"));
        System.out.println(map.get("Иннокентий"));
        System.out.println(map.get("Валера"));
        System.out.println(map.get("Ашот"));

        Set<String> set = new HashSetImpl<>();
        set.add("Намджун");
        set.add("Чонгук");
        set.add("Чингачгук");
        set.add("Чимин");
        set.add("Ким");
        set.add("Джамшут");
        set.add("Иван");
        System.out.println(set.contains("Намджун"));
        System.out.println(set.contains("Чонгук"));
        System.out.println(set.contains("Чимин"));
        System.out.println(set.contains("Джамшут"));
        System.out.println(set.contains("Иван"));
        System.out.println(set.contains("Камиль"));

    }
    }
