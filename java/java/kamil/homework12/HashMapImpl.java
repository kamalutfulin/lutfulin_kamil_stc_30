package kamil.homework12;

public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
    private MapEntry<K,V> newMapEntry(K key, V value){// TODO в чем ценнность этого метода?
        return new MapEntry<>(key,value);
    }
    private void processWithHash(K key, V value, int index){
        MapEntry<K,V> current = entries[index];
        if (current.key.equals(key)){
            current.value = value;
        } else {
            while (current.next != null && !current.key.equals(key)){
                current = current.next;
            }
        }
    }

    // TODO порядок методов тоже нужно соблюдать, вспомогательные методы должны быть ниже основного
    private int getHash(K key){ // TODO getIndex
        return  key.hashCode()&(entries.length-1);
    }

    @Override
    public void put(K key, V value) {
        int index = getHash(key);
        if (entries[index] == null){
            entries[index] = newMapEntry(key,value);
        } else {
            processWithHash(key,value,index); // TODO названия вроде process не несут никакого смысла
        }
    }

    @Override
    public V get(K key) { // TODO понятней не стало
        int index = getHash(key);
        if (entries[index] == null) {
            return null;
        }
        MapEntry<K, V> current = entries[index];
        if (current.key.equals(key)) {
            return current.value;
        }
        while (current.next != null && !current.key.equals(key)) {
            current = current.next;
        }
        if (current.key.equals(key)) {
            return current.value;
        } else {
            return null;
        }
    }

    public boolean contains(K key) {
       return get(key) != null;
    }

}


