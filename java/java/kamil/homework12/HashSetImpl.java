package kamil.homework12;

public class HashSetImpl<V> implements Set<V> {
    private HashMapImpl <V,Object> hashMap = new HashMapImpl<>();
    private static final Object DUMMY = new Object();

    @Override
    public void add(V value) {
        // TODO это не проще, а именно то что я хотел, и паттерн адаптер как он есть
        hashMap.put(value, DUMMY);
    }

    @Override
    public boolean contains(V value) {
        return hashMap.contains(value);
    }
}

