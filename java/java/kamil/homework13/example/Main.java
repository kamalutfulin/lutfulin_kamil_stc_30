package kamil.homework13.example;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("C:\\java-se-8u41-ri\\Innopolis\\lutfulin_kamil_stc_30\\java\\java\\kamil\\homework13\\example\\base.txt");
//        if (Files.exists(path)) {
//            Files.delete(path);
//        }
//        if (!Files.exists(path)) {
//            Files.createFile(path);
//        }
        DaoUser daoUser = new DaoUser(new Mapper(), path);

        User kamil = new User(1, "Kamil", "123");
        System.out.println(kamil);

        System.out.println(daoUser.save(kamil));
        System.out.println(daoUser.save(new User(2, "Denis", "456")));
        System.out.println(daoUser.save(new User(3, "Misha", "456")));
        System.out.println();

        User loadedUser = daoUser.findById(1);
        User loadedUser2 = daoUser.findByName(kamil.getName());

        System.out.println(kamil.equals(loadedUser));
        System.out.println(kamil.equals(loadedUser2));

        daoUser.deleteById(2);

        System.out.println(daoUser.findAll());

    }


}
