package kamil.homework13.Models;

public class ModelCourse {
    private String identif;
    private String nameCourse;
    private String dateOfStart;
    private String dateOfFinish;
    private String listOfTeachers;
    private String listOfLessonsCours;

    public String getIdentif() {
        return identif;
    }

    public void setIdentif(String identif) {
        this.identif = identif;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public String getDateOfStart() {
        return dateOfStart;
    }

    public void setDateOfStart(String dateOfStart) {
        this.dateOfStart = dateOfStart;
    }

    public String getDateOfFinish() {
        return dateOfFinish;
    }

    public void setDateOfFinish(String dateOfFinish) {
        this.dateOfFinish = dateOfFinish;
    }

    public String getListOfTeachers() {
        return listOfTeachers;
    }

    public void setListOfTeachers(String listOfTeachers) {
        this.listOfTeachers = listOfTeachers;
    }

    public String getListOfLessonsCours() {
        return listOfLessonsCours;
    }

    public void setListOfLessonsCours(String listOfLessonsCours) {
        this.listOfLessonsCours = listOfLessonsCours;
    }

    public ModelCourse(String identif, String nameCourse, String dateOfStart, String dateOfFinish, String listOfTeachers, String listOfLessonsCours) {
        this.identif = identif;
        this.nameCourse = nameCourse;
        this.dateOfStart = dateOfStart;
        this.dateOfFinish = dateOfFinish;
        this.listOfTeachers = listOfTeachers;
        this.listOfLessonsCours = listOfLessonsCours;
    }
}