package kamil.homework9version2;

import java.util.Arrays;

public class NumbersProcessorTest {

    private static NumbersAndStringProcessor processor;
    private static String[] strings;
    private static int[] ints;

    public static void main(String[] args) {




        System.out.println("processInvertTest " + processInvertTest());
        System.out.println("processInvertWithZerosTest " + processInvertWithZerosTest());
    }

    static void init() {
        strings = new String[]{"123", "456"};
        ints= new int[]{123, 456};
        processor = new NumbersAndStringProcessor(strings, ints);
    }

    static boolean processInvertTest() {
        init();
        boolean isWorkCorrect;

        int[] excpected = new int[] {321, 654};
        int[] actual = processor.process(new NumberInverter());

        isWorkCorrect = arrayEquals(excpected, actual);
        print(actual, excpected, isWorkCorrect);
        return isWorkCorrect;
    }

    static boolean processInvertWithZerosTest() {
        init();
        boolean isWorkCorrect;

        processor = new NumbersAndStringProcessor(null, new int[] {65450062, 51530});
        int[] excpected = new int[] {26005456, 3515};
        int[] actual = processor.process(new NumberInverter());

        isWorkCorrect = arrayEquals(excpected, actual);
        print(actual, excpected, isWorkCorrect);
        return isWorkCorrect;
    }

    static void print(int[] actual, int[] excpected, boolean isWorkCorrect) {
        if (!isWorkCorrect) {
            System.out.println("Actual: " + Arrays.toString(actual));
            System.out.println("Expected: " + Arrays.toString(excpected));
        }
    }static void print(String[] actual, String[] excpected, boolean isWorkCorrect) {
        if (!isWorkCorrect) {
            System.out.println("Actual: " + Arrays.toString(actual));
            System.out.println("Expected: " + Arrays.toString(excpected));
        }
    }

    static boolean arrayEquals(int[] array1, int[] array2) {
        if (array1.length != array2.length) {
            return false;
        }
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
}
