package kamil.homework9version2;

public class NumberInverter implements NumbersProcess {
    @Override
    public int process(int number) {

        int value = number; //TODO пермести переменную в цикл, потому что она нормально используется только там
        int[] nums = numToArr(value); // TODO сюда передавай number, зачем тебе посредники
        for (int i = 0; i < nums.length / 2; i++) {
            value = nums[i];
            nums[i] = nums[nums.length - i - 1];
            nums[nums.length - i - 1] = value;
        }
        return arrToNum(nums);
    }
    private static int[] numToArr(int num) { //  TODO можно сделать оптимальней и понятней, но за вынос в отдельный метод ставлю лайк, но это нужно разделить еще на 2 метода
        int value = num;
        int count = 0;
        int[] result;
        do {
            value = value / 10;
            count++;
        } while (value / 10 != 0 || value % 10 != 0);
        result = new int[count];
        value = num;
        for (int i = result.length - 1; i >= 0; i--) {
            result[i] = value % 10;
            value = value / 10;
        }
        return result;
    }

    private static int arrToNum(int[] arr) {
        int value = 0;
        for (int i = 0; i < arr.length; i++) {
            value = value * 10 + arr[i];
        }
        return value;
    }
}
