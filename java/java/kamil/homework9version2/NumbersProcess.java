package kamil.homework9version2;
@FunctionalInterface
public interface NumbersProcess {
    int process(int number);
}
